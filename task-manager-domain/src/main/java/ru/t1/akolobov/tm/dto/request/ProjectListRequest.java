package ru.t1.akolobov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.SortBy;


@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private SortBy sortType;

    public ProjectListRequest(@Nullable final String token) {
        super(token);
    }

}
