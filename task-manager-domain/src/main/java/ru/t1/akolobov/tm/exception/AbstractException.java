package ru.t1.akolobov.tm.exception;

import org.jetbrains.annotations.NotNull;

public class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
