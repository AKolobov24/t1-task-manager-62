package ru.t1.akolobov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(@Nullable final Role role) {
        if (role == null) return "";
        return role.getDisplayName();
    }

    @Nullable
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Role role : values()) {
            if (role.name().equals(value)) return role;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
