package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class ApplicationSystemInfoResponse extends AbstractResponse {

    @NotNull
    String hostName;

    int processors;

    long freeMemory;

    long maxMemory;

    long totalMemory;

}
