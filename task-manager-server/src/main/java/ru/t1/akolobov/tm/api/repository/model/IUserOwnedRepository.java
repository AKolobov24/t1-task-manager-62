package ru.t1.akolobov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.SortBy;
import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void add(@NotNull String userId, @NotNull M model);

    void update(@NotNull String userId, @NotNull M model);

    void clear(@NotNull String userId);

    boolean existById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull SortBy sortBy);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    Long getSize(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull M model);

    void removeById(@NotNull String userId, @NotNull String id);

}
