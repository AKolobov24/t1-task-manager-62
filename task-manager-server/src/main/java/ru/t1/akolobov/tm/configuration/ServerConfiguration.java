package ru.t1.akolobov.tm.configuration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseConnection;
import liquibase.database.DatabaseFactory;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.akolobov.tm.api.service.IDatabaseProperty;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.service.LoggerService;
import ru.t1.akolobov.tm.service.PropertyService;

import javax.jms.ConnectionFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@AllArgsConstructor
@EnableTransactionManagement
@ComponentScan("ru.t1.akolobov.tm")
@EnableJpaRepositories("ru.t1.akolobov.tm.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IDatabaseProperty databaseProperty;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperty.getDatabaseDriver());
        dataSource.setUrl(databaseProperty.getDatabaseUrl());
        dataSource.setUsername(databaseProperty.getDatabaseUser());
        dataSource.setPassword(databaseProperty.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.akolobov.tm.dto.model", "ru.t1.akolobov.tm.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseDDLAuto());
        properties.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSQL());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getSecondLvlCache());
        properties.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getConfigFilePath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @Nullable
    public ConnectionFactory connectionFactory(
            @NotNull @Autowired final IPropertyService propertyService,
            @NotNull @Autowired final LoggerService loggerService) {
        @NotNull final String url = propertyService.getExternalLoggerUrl();
        if (url.equals(propertyService.getEmptyValue())) {
            loggerService.info("WARNING: External logger url was not defined!");
            return null;
        }
        return new ActiveMQConnectionFactory(url);
    }

    @Bean
    @NotNull
    @SneakyThrows
    public Liquibase liquibase() {
        @NotNull final ResourceAccessor resourceAccessor =
                new ClassLoaderResourceAccessor(Thread.currentThread().getContextClassLoader());
        @NotNull final DatabaseConnection connection = DatabaseFactory
                .getInstance()
                .openConnection(
                        propertyService.getDatabaseUrl(),
                        propertyService.getDatabaseUser(),
                        propertyService.getDatabasePassword(),
                        null,
                        resourceAccessor
                );
        @NotNull final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(connection);
        return new Liquibase("changelog/changelog-master.xml", resourceAccessor, database);
    }

}
