package ru.t1.akolobov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.enumerated.Role;

import java.util.Date;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDto> {

    @NotNull
    SessionDto updateById(@Nullable String userId,
                          @Nullable String id,
                          @Nullable Date date,
                          @Nullable Role role);

}
