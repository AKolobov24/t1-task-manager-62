package ru.t1.akolobov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.service.IDomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class Backup {

    @NotNull
    @Autowired
    private final IDomainService domainService;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final IDomainService domainService) {
        this.domainService = domainService;
    }

    public void start() {
        es.scheduleWithFixedDelay(this::save, 0, 30, TimeUnit.SECONDS);
    }

    public void save() {
        domainService.saveDataBackup();
    }

    public void load() {
        if (!Files.exists(Paths.get(domainService.getBackupFilePath()))) return;
        domainService.loadDataBackup();
    }

    public void stop() {
        es.shutdown();
    }

}
