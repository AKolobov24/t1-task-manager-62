package ru.t1.akolobov.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.model.Session;

@Repository
@Scope("prototype")
public interface SessionRepository extends UserOwnedRepository<Session> {

}
