package ru.t1.akolobov.tm.api.component;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

}
