package ru.t1.akolobov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.component.ISaltProvider;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public interface HashUtil {

    @NotNull
    static String salt(
            @NotNull final String value,
            @NotNull final Integer iterations,
            @NotNull final String secret) {
        @NotNull String result = value;
        for (int i = 0; i < iterations; i++) {
            result = sha256(secret + result + secret);
        }
        return result;
    }

    static String salt(
            @NotNull final ISaltProvider saltProvider,
            @NotNull final String value) {
        @NotNull final String secret = saltProvider.getPasswordSecret();
        @NotNull final Integer iterations = saltProvider.getPasswordIteration();
        return salt(value, iterations, secret);
    }

    @NotNull
    static String sha256(@NotNull final String value) {
        try {
            @NotNull final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(value.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                @NotNull final String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
