package ru.t1.akolobov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.api.service.dto.IDtoService;
import ru.t1.akolobov.tm.dto.model.AbstractDtoModel;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.repository.dto.CommonDtoRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<M extends AbstractDtoModel> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected CommonDtoRepository<M> repository;

    @Override
    @Transactional
    public void add(final @NotNull M model) {
        repository.save(model);
    }

    @Override
    @Transactional
    public void update(final @NotNull M model) {
        repository.save(model);
    }

    @Override
    @Transactional
    public @NotNull Collection<M> add(final @NotNull Collection<M> models) {
        return repository.saveAll(models);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(final @NotNull Collection<M> models) {
        clear();
        return repository.saveAll(models);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @NotNull
    @Override
    public Optional<M> findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id);
    }

    @Override
    public Long getSize() {
        return repository.count();
    }

    @Override
    @Transactional
    public void remove(final @NotNull M model) {
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

}
