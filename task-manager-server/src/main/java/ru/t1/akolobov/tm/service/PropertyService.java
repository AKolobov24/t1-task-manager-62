package ru.t1.akolobov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.service.IDatabaseProperty;
import ru.t1.akolobov.tm.api.service.IPropertyService;

@Getter
@Setter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService, IDatabaseProperty {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    @Value("${password.secret}")
    private String passwordSecret;

    @NotNull
    @Value("${password.iteration:3123}")
    private Integer passwordIteration;

    @NotNull
    @Value("${server.port:6060}")
    private Integer serverPort;

    @NotNull
    @Value("${server.host:0.0.0.0}")
    private String serverHost;

    @NotNull
    @Value("${session.key:" + EMPTY_VALUE + "}")
    private String sessionKey;

    @Value("${session.timeout:900}")
    private int sessionTimeout;

    @NotNull
    @Value("${database.url:" + EMPTY_VALUE + "}")
    private String databaseUrl;

    @NotNull
    @Value("${database.username:" + EMPTY_VALUE + "}")
    private String databaseUser;

    @NotNull
    @Value("${database.password}")
    private String databasePassword;

    @NotNull
    @Value("${database.driver:" + EMPTY_VALUE + "}")
    private String databaseDriver;

    @NotNull
    @Value("${database.dialect:" + EMPTY_VALUE + "}")
    private String databaseDialect;

    @NotNull
    @Value("${database.ddl-auto:" + EMPTY_VALUE + "}")
    private String databaseDDLAuto;

    @NotNull
    @Value("${database.show-sql:" + EMPTY_VALUE + "}")
    private String databaseShowSQL;

    @NotNull
    @Value("${database.second-lvl-cache:" + EMPTY_VALUE + "}")
    private String secondLvlCache;

    @NotNull
    @Value("${database.factory-class:" + EMPTY_VALUE + "}")
    private String factoryClass;
    @NotNull
    @Value("${database.use-query-cache:" + EMPTY_VALUE + "}")
    private String useQueryCache;

    @NotNull
    @Value("${database.use-min-puts:" + EMPTY_VALUE + "}")
    private String useMinimalPuts;

    @NotNull
    @Value("${database.region-prefix:" + EMPTY_VALUE + "}")
    private String regionPrefix;

    @NotNull
    @Value("${database.config-file-path:" + EMPTY_VALUE + "}")
    private String configFilePath;

    @NotNull
    @Value("${external-logger.url:" + EMPTY_VALUE + "}")
    private String externalLoggerUrl;

    @NotNull
    @Value("${external-logger.queue:" + EMPTY_VALUE + "}")
    private String externalLoggerQueue;

    @NotNull
    @Override
    public String getEmptyValue() {
        return EMPTY_VALUE;
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getApplicationAuthor() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

}
