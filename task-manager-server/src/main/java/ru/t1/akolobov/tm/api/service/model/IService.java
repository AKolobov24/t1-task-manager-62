package ru.t1.akolobov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IService<M extends AbstractModel> {

    void add(@NotNull M model);

    void update(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void clear();

    @NotNull
    Optional<M> findOneById(@NotNull String id);

    boolean existsById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Sort sort);

    Long getSize();

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

}
