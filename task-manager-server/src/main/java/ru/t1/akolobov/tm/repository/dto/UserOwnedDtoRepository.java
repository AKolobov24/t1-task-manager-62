package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.akolobov.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface UserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel> extends CommonDtoRepository<M> {

    @NotNull
    Optional<M> findByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAllByUserId(@NotNull String userId);

    @NotNull
    List<M> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@NotNull String userId);

    long countByUserId(@NotNull String userId);

}
