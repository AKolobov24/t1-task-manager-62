package ru.t1.akolobov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import ru.t1.akolobov.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.List;
import java.util.Optional;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedDtoModel> extends IDtoService<M> {

    void add(@Nullable String userId, @Nullable M model);

    void update(@Nullable String userId, @Nullable M model);

    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    Optional<M> findOneById(@Nullable String userId, @Nullable String id);

    Long getSize(@Nullable String userId);

    void remove(@Nullable String userId, @Nullable M model);

    void removeById(@Nullable String userId, @Nullable String id);

}
