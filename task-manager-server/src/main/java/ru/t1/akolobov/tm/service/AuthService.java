package ru.t1.akolobov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.service.IAuthService;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.dto.ISessionDtoService;
import ru.t1.akolobov.tm.api.service.dto.IUserDtoService;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.exception.user.AccessDeniedException;
import ru.t1.akolobov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.akolobov.tm.exception.user.LoginEmptyException;
import ru.t1.akolobov.tm.exception.user.PasswordEmptyException;
import ru.t1.akolobov.tm.util.CryptUtil;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.Date;

@Service
@AllArgsConstructor
public final class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private final IUserDtoService userService;

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    @Autowired
    private final ISessionDtoService sessionService;

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDto user = userService.findByLogin(login)
                .orElseThrow(IncorrectLoginOrPasswordException::new);
        if (user.isLocked() || !HashUtil.salt(propertyService, password).equals(user.getPasswordHash()))
            throw new IncorrectLoginOrPasswordException();
        return getToken(user);
    }

    @NotNull
    private String getToken(@NotNull final UserDto user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDto session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(token, sessionKey);
    }

    @NotNull
    private SessionDto createSession(@NotNull UserDto user) {
        @NotNull final SessionDto session = new SessionDto();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        sessionService.add(session.getUserId(), session);
        return session;
    }

    @NotNull
    @SneakyThrows
    public SessionDto validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(token, sessionKey);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull SessionDto session = objectMapper.readValue(json, SessionDto.class);

        @NotNull Date currentDate = new Date();
        final long delta = (currentDate.getTime() - session.getDate().getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existById(session.getUserId(), session.getId())) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void logout(@Nullable String token) {
        if (token == null || token.isEmpty()) return;
        @NotNull final SessionDto session = validateToken(token);
        sessionService.removeById(session.getUserId(), session.getId());
    }

}
