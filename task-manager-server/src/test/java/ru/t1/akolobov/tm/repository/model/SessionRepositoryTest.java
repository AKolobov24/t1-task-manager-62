package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.data.model.TestSession;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Session;

import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);
    @NotNull
    private final static UserRepository userRepository = context.getBean(UserRepository.class);

    @NotNull
    private final static SessionRepository repository = context.getBean(SessionRepository.class);

    @BeforeClass
    public static void prepareConnection() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @After
    public void deleteByUserIdData() {
        repository.deleteByUserId(USER1.getId());
        repository.deleteByUserId(USER2.getId());
    }

    @Test
    public void save() {
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Session session = TestSession.createSession(USER1);
        repository.save(session);
        Assert.assertEquals(session, repository.findAll().get(0));
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void deleteByUserId() {
        @NotNull final List<Session> sessionList = TestSession.createSessionList(USER1);
        sessionList.forEach(repository::save);
        @NotNull final Session user2Session = TestSession.createSession(USER2);
        repository.save(user2Session);
        Assert.assertEquals(sessionList.size() + 1, repository.findAll().size());
        repository.deleteByUserId(USER1_ID);
        Assert.assertEquals(1, repository.findAll().size());
        Assert.assertEquals(user2Session, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        @NotNull final Session session = TestSession.createSession(USER1);
        repository.save(session);
        Assert.assertTrue(repository.existsByUserIdAndId(USER1_ID, session.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(USER2_ID, session.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Session> user1SessionList = TestSession.createSessionList(USER1);
        @NotNull final List<Session> user2SessionList = TestSession.createSessionList(USER2);
        user1SessionList.forEach(repository::save);
        user2SessionList.forEach(repository::save);
        Assert.assertEquals(user1SessionList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2SessionList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final Session session = TestSession.createSession(USER1);
        repository.save(session);
        @NotNull final String sessionId = session.getId();
        Assert.assertEquals(session, repository.findByUserIdAndId(USER1_ID, sessionId).orElse(null));
        Assert.assertNull(repository.findByUserIdAndId(USER2_ID, sessionId).orElse(null));
    }

    @Test
    public void getSize() {
        @NotNull final List<Session> sessionList = TestSession.createSessionList(USER1);
        sessionList.forEach(repository::save);
        Assert.assertEquals(sessionList.size(), repository.countByUserId(USER1_ID));
        repository.save(TestSession.createSession(USER1));
        Assert.assertEquals((sessionList.size() + 1), repository.countByUserId(USER1_ID));
    }

    @Test
    public void delete() {
        TestSession.createSessionList(USER1).forEach(repository::save);
        @NotNull final Session session = TestSession.createSession(USER1);
        repository.save(session);
        Assert.assertEquals(session, repository.findByUserIdAndId(USER1_ID, session.getId()).orElse(null));
        repository.delete(session);
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, session.getId()).orElse(null));
    }

    @Test
    public void deleteById() {
        TestSession.createSessionList(USER1).forEach(repository::save);
        @NotNull final Session session = TestSession.createSession(USER1);
        repository.save(session);
        repository.deleteByUserIdAndId(USER1_ID, session.getId());
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, session.getId()).orElse(null));
    }

}
