package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.data.model.TestProject;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;

import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);
    @NotNull
    private final static UserRepository userRepository = context.getBean(UserRepository.class);

    @NotNull
    private final static ProjectRepository repository = context.getBean(ProjectRepository.class);


    @BeforeClass
    public static void prepareConnection() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @After
    public void deleteByUserIdData() {
        repository.deleteByUserId(USER1.getId());
        repository.deleteByUserId(USER2.getId());
    }

    @Test
    public void save() {
        @NotNull final Project project = TestProject.createProject(USER1);
        repository.save(project);
        Assert.assertTrue(repository.findAllByUserId(USER1_ID).contains(project));
        Assert.assertEquals(1, repository.findAllByUserId(USER1_ID).size());
    }

    @Test
    public void deleteByUserId() {
        @NotNull final List<Project> projectList = TestProject.createProjectList(USER1);
        projectList.forEach(repository::save);
        long size = repository.count();
        repository.deleteByUserId(USER1_ID);
        Assert.assertEquals(
                size - projectList.size(),
                repository.count()
        );
    }

    @Test
    public void existsByUserIdAndId() {
        @NotNull final Project project = TestProject.createProject(USER1);
        repository.save(project);
        Assert.assertTrue(repository.existsByUserIdAndId(USER1_ID, project.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(USER2_ID, project.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Project> user1ProjectList = TestProject.createProjectList(USER1);
        @NotNull final List<Project> user2ProjectList = TestProject.createProjectList(USER2);
        user1ProjectList.forEach(repository::save);
        user2ProjectList.forEach(repository::save);
        Assert.assertEquals(user1ProjectList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2ProjectList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findByUserIdAndId() {
        @NotNull final Project project = TestProject.createProject(USER1);
        repository.save(project);
        @NotNull final String projectId = project.getId();
        Assert.assertEquals(project, repository.findByUserIdAndId(USER1_ID, projectId).orElse(null));
        Assert.assertNull(repository.findByUserIdAndId(USER2_ID, projectId).orElse(null));
    }

    @Test
    public void count() {
        @NotNull final List<Project> projectList = TestProject.createProjectList(USER1);
        projectList.forEach(repository::save);
        Assert.assertEquals(projectList.size(), repository.countByUserId(USER1_ID));
        repository.save(TestProject.createProject(USER1));
        Assert.assertEquals((projectList.size() + 1), repository.countByUserId(USER1_ID));
    }

    @Test
    public void delete() {
        TestProject.createProjectList(USER1).forEach(repository::save);
        @NotNull final Project project = TestProject.createProject(USER1);
        repository.save(project);
        Assert.assertEquals(project, repository.findByUserIdAndId(USER1_ID, project.getId()).orElse(null));
        repository.delete(project);
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, project.getId()).orElse(null));
    }

    @Test
    public void deleteById() {
        TestProject.createProjectList(USER1).forEach(repository::save);
        @NotNull final Project project = TestProject.createProject(USER1);
        repository.save(project);
        repository.deleteByUserIdAndId(USER1_ID, project.getId());
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, project.getId()).orElse(null));
    }

}
