package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.User;

import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);
    @NotNull
    private final static UserRepository repository = context.getBean(UserRepository.class);

    @Test
    public void save() {
        repository.save(USER1);
        Assert.assertTrue(repository.findAll().contains(USER1));
        repository.delete(USER1);
    }

    @Test
    @Ignore
    public void deleteAll() {
        @NotNull final List<User> userList = createUserList();
        userList.forEach(repository::save);
        Assert.assertEquals(userList.size(), repository.findAll().size());
        repository.deleteAll();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void existsById() {
        repository.save(USER1);
        Assert.assertTrue(repository.existsById(USER1.getId()));
        Assert.assertFalse(repository.existsById(USER2.getId()));
        repository.delete(USER1);
    }

    @Test
    public void findAll() {
        @NotNull final List<User> user1UserList = createUserList();
        user1UserList.forEach(repository::save);
        Assert.assertTrue(repository.findAll().containsAll(user1UserList));
        user1UserList.forEach(repository::delete);
    }

    @Test
    public void findById() {
        repository.save(USER1);
        Assert.assertEquals(USER1, repository.findById(USER1.getId()).orElse(null));
        Assert.assertNull(repository.findById(USER2.getId()).orElse(null));
        repository.delete(USER1);
    }

    @Test
    public void count() {
        @NotNull final List<User> userList = repository.findAll();
        Assert.assertEquals(userList.size(), repository.count());
        repository.save(NEW_USER);
        Assert.assertEquals((userList.size() + 1), repository.count());
        repository.delete(NEW_USER);
    }

    @Test
    public void delete() {
        repository.save(NEW_USER);
        Assert.assertEquals(NEW_USER, repository.findById(NEW_USER.getId()).orElse(null));
        final long size = repository.count();
        repository.delete(NEW_USER);
        Assert.assertNull(repository.findById(NEW_USER.getId()).orElse(null));
        Assert.assertEquals(size - 1, repository.count());
    }

    @Test
    public void deleteById() {
        repository.save(NEW_USER);
        Assert.assertNotNull(repository.findById(NEW_USER.getId()));
        repository.deleteById(NEW_USER.getId());
        Assert.assertNull(repository.findById(NEW_USER.getId()).orElse(null));
    }

    @Test
    public void findByLogin() {
        repository.save(USER1);
        Assert.assertEquals(USER1, repository.findByLogin(USER1.getLogin()).orElse(null));
        Assert.assertNull(repository.findById(USER2.getLogin()).orElse(null));
        repository.delete(USER1);
    }

    @Test
    public void findByEmail() {
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.save(USER1);
        Assert.assertEquals(USER1, repository.findByEmail(USER1.getEmail()).orElse(null));
        Assert.assertNull(repository.findByEmail("user_2@email.ru").orElse(null));
        repository.delete(USER1);
    }

    @Test
    public void existsByLogin() {
        repository.save(USER1);
        Assert.assertTrue(repository.existsByLogin(USER1.getLogin()));
        Assert.assertFalse(repository.existsByLogin(USER2.getLogin()));
        repository.delete(USER1);
    }

    @Test
    public void existsByEmail() {
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.save(USER1);
        Assert.assertTrue(repository.existsByEmail(USER1.getEmail()));
        Assert.assertFalse(repository.existsByEmail("user_2@email.ru"));
        repository.delete(USER1);
    }

}
