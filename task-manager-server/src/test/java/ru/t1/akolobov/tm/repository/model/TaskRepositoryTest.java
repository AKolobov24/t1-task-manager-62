package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.data.model.TestTask;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);
    @NotNull
    private final static UserRepository userRepository = context.getBean(UserRepository.class);

    @NotNull
    private final static TaskRepository repository = context.getBean(TaskRepository.class);

    @NotNull
    private final static ProjectRepository projectRepository = context.getBean(ProjectRepository.class);

    @BeforeClass
    public static void prepareConnection() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @After
    public void clearData() {
        repository.deleteByUserId(USER1.getId());
        repository.deleteByUserId(USER2.getId());
    }

    @Test
    public void save() {
        @NotNull final Task task = TestTask.createTask(USER1);
        repository.save(task);
        Assert.assertEquals(task, repository.findAllByUserId(USER1_ID).get(0));
        Assert.assertEquals(1, repository.findAllByUserId(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull final List<Task> TaskList = TestTask.createTaskList(USER1);
        TaskList.forEach(repository::save);
        long size = repository.count();
        repository.deleteByUserId(USER1_ID);
        Assert.assertEquals(
                size - TaskList.size(),
                repository.count()
        );
    }

    @Test
    public void existById() {
        @NotNull final Task task = TestTask.createTask(USER1);
        repository.save(task);
        Assert.assertTrue(repository.existsByUserIdAndId(USER1_ID, task.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(USER2_ID, task.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> user1TaskList = TestTask.createTaskList(USER1);
        @NotNull final List<Task> user2TaskList = TestTask.createTaskList(USER2);
        user1TaskList.forEach(repository::save);
        user2TaskList.forEach(repository::save);
        Assert.assertEquals(user1TaskList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2TaskList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findByUserIdAndId() {
        @NotNull final Task task = TestTask.createTask(USER1);
        repository.save(task);
        @NotNull final String taskId = task.getId();
        Assert.assertEquals(task, repository.findByUserIdAndId(USER1_ID, taskId).orElse(null));
        Assert.assertNull(repository.findByUserIdAndId(USER2_ID, taskId).orElse(null));
    }

    @Test
    public void getSize() {
        @NotNull final List<Task> taskList = TestTask.createTaskList(USER1);
        taskList.forEach(repository::save);
        Assert.assertEquals(taskList.size(), repository.countByUserId(USER1_ID));
        repository.save(TestTask.createTask(USER1));
        Assert.assertEquals((taskList.size() + 1), repository.countByUserId(USER1_ID));
    }

    @Test
    public void remove() {
        TestTask.createTaskList(USER1).forEach(repository::save);
        @NotNull final Task task = TestTask.createTask(USER1);
        repository.save(task);
        Assert.assertEquals(task, repository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null));
        repository.delete(task);
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null));
    }

    @Test
    public void removeById() {
        TestTask.createTaskList(USER1).forEach(repository::save);
        @NotNull final Task task = TestTask.createTask(USER1);
        repository.save(task);
        repository.deleteByUserIdAndId(USER1_ID, task.getId());
        Assert.assertNull(repository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null));
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final Project project = new Project("test-project-for-task");
        project.setUser(USER1);
        projectRepository.save(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<Task> taskList = TestTask.createTaskList(USER1);
        taskList.forEach(t -> t.setProject(project));
        taskList.forEach(repository::save);
        repository.save(TestTask.createTask(USER1));
        Assert.assertEquals(taskList, repository.findAllByUserIdAndProjectId(USER1_ID, projectId));
        taskList.forEach(repository::delete);
        projectRepository.deleteByUserIdAndId(USER1_ID, projectId);
    }


}
