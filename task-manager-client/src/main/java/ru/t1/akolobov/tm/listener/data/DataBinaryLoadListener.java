package ru.t1.akolobov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataBinaryLoadRequest;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Component
public final class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-binary";

    @NotNull
    public static final String DESCRIPTION = "Load data from binary file.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataBinaryLoadListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA LOAD BINARY]");
        getDomainEndpoint().loadDataBinary(new DataBinaryLoadRequest(getToken()));
    }

}
