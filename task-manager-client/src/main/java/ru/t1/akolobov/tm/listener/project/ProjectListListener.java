package ru.t1.akolobov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.dto.request.ProjectListRequest;
import ru.t1.akolobov.tm.dto.response.ProjectListResponse;
import ru.t1.akolobov.tm.enumerated.SortBy;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Display list of all projects.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectListListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(SortBy.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSortType(
                Optional.ofNullable(SortBy.toSort(sortType))
                        .orElse(SortBy.BY_CREATED)
        );
        @NotNull final ProjectListResponse response = getProjectEndpoint().list(request);
        @NotNull final List<ProjectDto> projectList = response.getProjectList();
        int index = 1;
        for (@NotNull final ProjectDto project : projectList) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
