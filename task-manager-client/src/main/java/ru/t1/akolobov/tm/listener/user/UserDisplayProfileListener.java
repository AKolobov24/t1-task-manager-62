package ru.t1.akolobov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.dto.request.UserProfileRequest;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.exception.user.UserNotFoundException;

@Component
public final class UserDisplayProfileListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-display-profile";

    @NotNull
    public static final String DESCRIPTION = "Display current user profile.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userDisplayProfileListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DISPLAY USER PROFILE]");
        @Nullable final UserDto user = getAuthEndpoint().profile(new UserProfileRequest(getToken())).getUser();
        if (user == null) throw new UserNotFoundException();
        displayUser(user);
    }

    @Override
    protected void displayUser(@Nullable final UserDto user) {
        super.displayUser(user);
        if (user == null) throw new UserNotFoundException();
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }

}
