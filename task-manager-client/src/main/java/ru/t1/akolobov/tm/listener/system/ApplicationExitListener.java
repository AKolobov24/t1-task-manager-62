package ru.t1.akolobov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "exit";

    @NotNull
    public static final String DESCRIPTION = "Exit application.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@applicationExitListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

}
