package ru.t1.akolobov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataXmlLoadFasterXmlRequest;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlLoadFasterXmlListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA LOAD XML]");
        getDomainEndpoint().loadDataXmlFasterXml(new DataXmlLoadFasterXmlRequest(getToken()));
    }

}
