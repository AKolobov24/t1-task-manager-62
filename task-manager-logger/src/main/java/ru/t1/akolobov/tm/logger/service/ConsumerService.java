package ru.t1.akolobov.tm.logger.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.logger.api.IConsumerService;

import javax.jms.*;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ConsumerService implements IConsumerService {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @NotNull
    @Autowired
    private MessageListener messageListener;

    @Override
    @SneakyThrows
    public void subscribe(@NotNull final String queue) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        @NotNull final Session session;
        @NotNull final Queue destination;

        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(queue);
        @NotNull final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(messageListener);
    }

}
